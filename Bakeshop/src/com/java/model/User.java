package com.java.model;

public class User {
	
	private String empName;
	private int empId;
	private int storeId;
	private String empRole;
	
	public User(String empName, int empId) {
		this.empName = empName;
		this.empId = empId;
	}
	
	public User(String empName, int empId,String empRole) {
		this.empName = empName;
		this.empId = empId;
		this.empRole = empRole;
	}
	
	public String getEmpRole() {
		return empRole;
	}

	public void setEmpRole(String empRole) {
		this.empRole = empRole;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	
	
	
}
