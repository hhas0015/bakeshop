package com.java.model;

public class Inventory {
	
	private String itemNo;
	private String itemName;
	private int price;
	private int quantity;
	
	public Inventory(String itemNo, String itemName, int price, int quantity) {
		super();
		this.itemNo = itemNo;
		this.itemName = itemName;
		this.price = price;
		this.quantity = quantity;
	}
	
	public String getItemNo() {
		return itemNo;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Inventory [itemNo=" + itemNo + ", itemName=" + itemName + ", price=" + price + ", quantity=" + quantity
				+ "]";
	}
	
}
