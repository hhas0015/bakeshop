package com.java.gui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.SwingConstants;

import com.java.model.User;

import java.awt.GridLayout;
import java.awt.Insets;

import com.java.config.*;

public class Login extends JFrame {

	private JFrame frame;
	private JTextField email;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(119, 136, 153));
		frame.setBounds(10, 10, 1450, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		email = new JTextField();
		email.setBounds(672, 203, 207, 39);
		frame.getContentPane().add(email);
		email.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email:");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(553, 213, 56, 16);
		frame.getContentPane().add(lblNewLabel);
		
		password = new JPasswordField();
		password.setBounds(672, 308, 207, 39);
		frame.getContentPane().add(password);
		
		JLabel lblNewLabel_1 = new JLabel("Password:");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(553, 319, 91, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton loginBtn = new JButton("Login");
		loginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String emailId = email.getText();
				String passwordVal = password.getText();
				
				try {
					
                    Connection connection = DatabaseConnection.getDbConnection();

                    PreparedStatement st = (PreparedStatement) connection
                        .prepareStatement("select email, password,emplyeeName,employeId,r.roleName from user u join roles r on u.roleId = r.roleId where email=? and password=?");
                    
                    

                    st.setString(1, emailId);
                    st.setString(2, passwordVal);
                    ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        frame.dispose();
                        String empName = rs.getString(3);
                        int empId = rs.getInt(4);
                        String empRole = rs.getString(5);
                        User loggedInUser = new User(empName, empId, empRole);
                        Dashboard dash = new Dashboard(loggedInUser);
                        //dash.setVisible(true);
                        //JOptionPane.showMessageDialog(frame, "You have successfully logged in");
                    } else {
                        JOptionPane.showMessageDialog(frame, "Wrong Username & Password");
                    }
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
				
			}
		});
		loginBtn.setForeground(Color.WHITE);
		loginBtn.setBackground(Color.ORANGE);
		loginBtn.setBounds(672, 425, 159, 39);
		frame.getContentPane().add(loginBtn);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.ORANGE);
		panel.setForeground(Color.ORANGE);
		panel.setBounds(0, 0, 1432, 100);
		frame.getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{16, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 112, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 49, 0};
		gbl_panel.rowHeights = new int[]{28, 29, 50, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel("");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.fill = GridBagConstraints.BOTH;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel.add(label, gbc_label);
		
		JLabel label_1 = new JLabel("");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.fill = GridBagConstraints.BOTH;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 0;
		panel.add(label_1, gbc_label_1);
		
		JLabel label_2 = new JLabel("");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.fill = GridBagConstraints.BOTH;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 2;
		gbc_label_2.gridy = 0;
		panel.add(label_2, gbc_label_2);
		
		JLabel label_3 = new JLabel("");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.fill = GridBagConstraints.BOTH;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 3;
		gbc_label_3.gridy = 0;
		panel.add(label_3, gbc_label_3);
		
		JLabel label_4 = new JLabel("");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.fill = GridBagConstraints.BOTH;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 4;
		gbc_label_4.gridy = 0;
		panel.add(label_4, gbc_label_4);
		
		JLabel label_5 = new JLabel("");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.fill = GridBagConstraints.BOTH;
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 5;
		gbc_label_5.gridy = 0;
		panel.add(label_5, gbc_label_5);
		
		JLabel label_6 = new JLabel("");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.fill = GridBagConstraints.BOTH;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 6;
		gbc_label_6.gridy = 0;
		panel.add(label_6, gbc_label_6);
		
		JLabel label_7 = new JLabel("");
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.fill = GridBagConstraints.BOTH;
		gbc_label_7.insets = new Insets(0, 0, 5, 5);
		gbc_label_7.gridx = 7;
		gbc_label_7.gridy = 0;
		panel.add(label_7, gbc_label_7);
		
		JLabel label_8 = new JLabel("");
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.fill = GridBagConstraints.BOTH;
		gbc_label_8.insets = new Insets(0, 0, 5, 5);
		gbc_label_8.gridx = 8;
		gbc_label_8.gridy = 0;
		panel.add(label_8, gbc_label_8);
		
		JLabel label_9 = new JLabel("");
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.fill = GridBagConstraints.BOTH;
		gbc_label_9.insets = new Insets(0, 0, 5, 5);
		gbc_label_9.gridx = 9;
		gbc_label_9.gridy = 0;
		panel.add(label_9, gbc_label_9);
		
		JLabel label_10 = new JLabel("");
		GridBagConstraints gbc_label_10 = new GridBagConstraints();
		gbc_label_10.fill = GridBagConstraints.BOTH;
		gbc_label_10.insets = new Insets(0, 0, 5, 5);
		gbc_label_10.gridx = 10;
		gbc_label_10.gridy = 0;
		panel.add(label_10, gbc_label_10);
		
		JLabel label_11 = new JLabel("");
		GridBagConstraints gbc_label_11 = new GridBagConstraints();
		gbc_label_11.fill = GridBagConstraints.BOTH;
		gbc_label_11.insets = new Insets(0, 0, 5, 5);
		gbc_label_11.gridx = 11;
		gbc_label_11.gridy = 0;
		panel.add(label_11, gbc_label_11);
		
		JLabel label_12 = new JLabel("");
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.fill = GridBagConstraints.BOTH;
		gbc_label_12.insets = new Insets(0, 0, 5, 5);
		gbc_label_12.gridx = 12;
		gbc_label_12.gridy = 0;
		panel.add(label_12, gbc_label_12);
		
		JLabel label_13 = new JLabel("");
		GridBagConstraints gbc_label_13 = new GridBagConstraints();
		gbc_label_13.fill = GridBagConstraints.BOTH;
		gbc_label_13.insets = new Insets(0, 0, 5, 5);
		gbc_label_13.gridx = 13;
		gbc_label_13.gridy = 0;
		panel.add(label_13, gbc_label_13);
		
		JLabel label_14 = new JLabel("");
		GridBagConstraints gbc_label_14 = new GridBagConstraints();
		gbc_label_14.fill = GridBagConstraints.BOTH;
		gbc_label_14.insets = new Insets(0, 0, 5, 5);
		gbc_label_14.gridx = 14;
		gbc_label_14.gridy = 0;
		panel.add(label_14, gbc_label_14);
		
		JLabel label_15 = new JLabel("");
		GridBagConstraints gbc_label_15 = new GridBagConstraints();
		gbc_label_15.fill = GridBagConstraints.BOTH;
		gbc_label_15.insets = new Insets(0, 0, 5, 5);
		gbc_label_15.gridx = 15;
		gbc_label_15.gridy = 0;
		panel.add(label_15, gbc_label_15);
		
		JLabel label_16 = new JLabel("");
		GridBagConstraints gbc_label_16 = new GridBagConstraints();
		gbc_label_16.fill = GridBagConstraints.BOTH;
		gbc_label_16.insets = new Insets(0, 0, 5, 5);
		gbc_label_16.gridx = 17;
		gbc_label_16.gridy = 0;
		panel.add(label_16, gbc_label_16);
		
		JLabel label_17 = new JLabel("");
		GridBagConstraints gbc_label_17 = new GridBagConstraints();
		gbc_label_17.fill = GridBagConstraints.BOTH;
		gbc_label_17.insets = new Insets(0, 0, 5, 5);
		gbc_label_17.gridx = 18;
		gbc_label_17.gridy = 0;
		panel.add(label_17, gbc_label_17);
		
		JLabel label_18 = new JLabel("");
		GridBagConstraints gbc_label_18 = new GridBagConstraints();
		gbc_label_18.fill = GridBagConstraints.BOTH;
		gbc_label_18.insets = new Insets(0, 0, 5, 5);
		gbc_label_18.gridx = 19;
		gbc_label_18.gridy = 0;
		panel.add(label_18, gbc_label_18);
		
		JLabel label_19 = new JLabel("");
		GridBagConstraints gbc_label_19 = new GridBagConstraints();
		gbc_label_19.fill = GridBagConstraints.BOTH;
		gbc_label_19.insets = new Insets(0, 0, 5, 5);
		gbc_label_19.gridx = 20;
		gbc_label_19.gridy = 0;
		panel.add(label_19, gbc_label_19);
		
		JLabel label_20 = new JLabel("");
		GridBagConstraints gbc_label_20 = new GridBagConstraints();
		gbc_label_20.fill = GridBagConstraints.BOTH;
		gbc_label_20.insets = new Insets(0, 0, 5, 5);
		gbc_label_20.gridx = 21;
		gbc_label_20.gridy = 0;
		panel.add(label_20, gbc_label_20);
		
		JLabel label_21 = new JLabel("");
		GridBagConstraints gbc_label_21 = new GridBagConstraints();
		gbc_label_21.fill = GridBagConstraints.BOTH;
		gbc_label_21.insets = new Insets(0, 0, 5, 5);
		gbc_label_21.gridx = 22;
		gbc_label_21.gridy = 0;
		panel.add(label_21, gbc_label_21);
		
		JLabel label_22 = new JLabel("");
		GridBagConstraints gbc_label_22 = new GridBagConstraints();
		gbc_label_22.fill = GridBagConstraints.BOTH;
		gbc_label_22.insets = new Insets(0, 0, 5, 5);
		gbc_label_22.gridx = 23;
		gbc_label_22.gridy = 0;
		panel.add(label_22, gbc_label_22);
		
		JLabel label_23 = new JLabel("");
		GridBagConstraints gbc_label_23 = new GridBagConstraints();
		gbc_label_23.fill = GridBagConstraints.BOTH;
		gbc_label_23.insets = new Insets(0, 0, 5, 5);
		gbc_label_23.gridx = 24;
		gbc_label_23.gridy = 0;
		panel.add(label_23, gbc_label_23);
		
		JLabel label_24 = new JLabel("");
		GridBagConstraints gbc_label_24 = new GridBagConstraints();
		gbc_label_24.fill = GridBagConstraints.BOTH;
		gbc_label_24.insets = new Insets(0, 0, 5, 5);
		gbc_label_24.gridx = 25;
		gbc_label_24.gridy = 0;
		panel.add(label_24, gbc_label_24);
		
		JLabel label_25 = new JLabel("");
		GridBagConstraints gbc_label_25 = new GridBagConstraints();
		gbc_label_25.fill = GridBagConstraints.BOTH;
		gbc_label_25.insets = new Insets(0, 0, 5, 5);
		gbc_label_25.gridx = 26;
		gbc_label_25.gridy = 0;
		panel.add(label_25, gbc_label_25);
		
		JLabel label_26 = new JLabel("");
		GridBagConstraints gbc_label_26 = new GridBagConstraints();
		gbc_label_26.fill = GridBagConstraints.BOTH;
		gbc_label_26.insets = new Insets(0, 0, 5, 5);
		gbc_label_26.gridx = 27;
		gbc_label_26.gridy = 0;
		panel.add(label_26, gbc_label_26);
		
		JLabel label_27 = new JLabel("");
		GridBagConstraints gbc_label_27 = new GridBagConstraints();
		gbc_label_27.fill = GridBagConstraints.BOTH;
		gbc_label_27.insets = new Insets(0, 0, 5, 0);
		gbc_label_27.gridx = 28;
		gbc_label_27.gridy = 0;
		panel.add(label_27, gbc_label_27);
		
		JLabel lblNewLabel_2 = new JLabel("Bake Shop");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 24));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 16;
		gbc_lblNewLabel_2.gridy = 1;
		panel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel label_28 = new JLabel("");
		GridBagConstraints gbc_label_28 = new GridBagConstraints();
		gbc_label_28.fill = GridBagConstraints.BOTH;
		gbc_label_28.insets = new Insets(0, 0, 0, 5);
		gbc_label_28.gridx = 0;
		gbc_label_28.gridy = 2;
		panel.add(label_28, gbc_label_28);
		
		JLabel label_29 = new JLabel("");
		GridBagConstraints gbc_label_29 = new GridBagConstraints();
		gbc_label_29.fill = GridBagConstraints.BOTH;
		gbc_label_29.insets = new Insets(0, 0, 0, 5);
		gbc_label_29.gridx = 1;
		gbc_label_29.gridy = 2;
		panel.add(label_29, gbc_label_29);
		
		JLabel label_30 = new JLabel("");
		GridBagConstraints gbc_label_30 = new GridBagConstraints();
		gbc_label_30.fill = GridBagConstraints.BOTH;
		gbc_label_30.insets = new Insets(0, 0, 0, 5);
		gbc_label_30.gridx = 2;
		gbc_label_30.gridy = 2;
		panel.add(label_30, gbc_label_30);
		
		JLabel label_31 = new JLabel("");
		GridBagConstraints gbc_label_31 = new GridBagConstraints();
		gbc_label_31.fill = GridBagConstraints.BOTH;
		gbc_label_31.insets = new Insets(0, 0, 0, 5);
		gbc_label_31.gridx = 3;
		gbc_label_31.gridy = 2;
		panel.add(label_31, gbc_label_31);
		
		JLabel label_32 = new JLabel("");
		GridBagConstraints gbc_label_32 = new GridBagConstraints();
		gbc_label_32.fill = GridBagConstraints.BOTH;
		gbc_label_32.insets = new Insets(0, 0, 0, 5);
		gbc_label_32.gridx = 4;
		gbc_label_32.gridy = 2;
		panel.add(label_32, gbc_label_32);
		
		JLabel label_33 = new JLabel("");
		GridBagConstraints gbc_label_33 = new GridBagConstraints();
		gbc_label_33.fill = GridBagConstraints.BOTH;
		gbc_label_33.insets = new Insets(0, 0, 0, 5);
		gbc_label_33.gridx = 5;
		gbc_label_33.gridy = 2;
		panel.add(label_33, gbc_label_33);
		
		JLabel label_34 = new JLabel("");
		GridBagConstraints gbc_label_34 = new GridBagConstraints();
		gbc_label_34.fill = GridBagConstraints.BOTH;
		gbc_label_34.insets = new Insets(0, 0, 0, 5);
		gbc_label_34.gridx = 6;
		gbc_label_34.gridy = 2;
		panel.add(label_34, gbc_label_34);
		
		JLabel label_35 = new JLabel("");
		GridBagConstraints gbc_label_35 = new GridBagConstraints();
		gbc_label_35.fill = GridBagConstraints.BOTH;
		gbc_label_35.insets = new Insets(0, 0, 0, 5);
		gbc_label_35.gridx = 7;
		gbc_label_35.gridy = 2;
		panel.add(label_35, gbc_label_35);
		
		JLabel label_36 = new JLabel("");
		GridBagConstraints gbc_label_36 = new GridBagConstraints();
		gbc_label_36.fill = GridBagConstraints.BOTH;
		gbc_label_36.insets = new Insets(0, 0, 0, 5);
		gbc_label_36.gridx = 8;
		gbc_label_36.gridy = 2;
		panel.add(label_36, gbc_label_36);
		
		JLabel label_37 = new JLabel("");
		GridBagConstraints gbc_label_37 = new GridBagConstraints();
		gbc_label_37.fill = GridBagConstraints.BOTH;
		gbc_label_37.insets = new Insets(0, 0, 0, 5);
		gbc_label_37.gridx = 9;
		gbc_label_37.gridy = 2;
		panel.add(label_37, gbc_label_37);
		
		JLabel label_38 = new JLabel("");
		GridBagConstraints gbc_label_38 = new GridBagConstraints();
		gbc_label_38.fill = GridBagConstraints.BOTH;
		gbc_label_38.insets = new Insets(0, 0, 0, 5);
		gbc_label_38.gridx = 10;
		gbc_label_38.gridy = 2;
		panel.add(label_38, gbc_label_38);
		
		JLabel label_39 = new JLabel("");
		GridBagConstraints gbc_label_39 = new GridBagConstraints();
		gbc_label_39.fill = GridBagConstraints.BOTH;
		gbc_label_39.insets = new Insets(0, 0, 0, 5);
		gbc_label_39.gridx = 11;
		gbc_label_39.gridy = 2;
		panel.add(label_39, gbc_label_39);
		
		JLabel label_40 = new JLabel("");
		GridBagConstraints gbc_label_40 = new GridBagConstraints();
		gbc_label_40.fill = GridBagConstraints.BOTH;
		gbc_label_40.insets = new Insets(0, 0, 0, 5);
		gbc_label_40.gridx = 12;
		gbc_label_40.gridy = 2;
		panel.add(label_40, gbc_label_40);
		
		JLabel label_41 = new JLabel("");
		GridBagConstraints gbc_label_41 = new GridBagConstraints();
		gbc_label_41.fill = GridBagConstraints.BOTH;
		gbc_label_41.insets = new Insets(0, 0, 0, 5);
		gbc_label_41.gridx = 13;
		gbc_label_41.gridy = 2;
		panel.add(label_41, gbc_label_41);
		
		JLabel label_42 = new JLabel("");
		GridBagConstraints gbc_label_42 = new GridBagConstraints();
		gbc_label_42.fill = GridBagConstraints.BOTH;
		gbc_label_42.insets = new Insets(0, 0, 0, 5);
		gbc_label_42.gridx = 14;
		gbc_label_42.gridy = 2;
		panel.add(label_42, gbc_label_42);
		
		JLabel label_43 = new JLabel("");
		GridBagConstraints gbc_label_43 = new GridBagConstraints();
		gbc_label_43.fill = GridBagConstraints.BOTH;
		gbc_label_43.insets = new Insets(0, 0, 0, 5);
		gbc_label_43.gridx = 15;
		gbc_label_43.gridy = 2;
		panel.add(label_43, gbc_label_43);
		
		JLabel label_44 = new JLabel("");
		GridBagConstraints gbc_label_44 = new GridBagConstraints();
		gbc_label_44.fill = GridBagConstraints.BOTH;
		gbc_label_44.insets = new Insets(0, 0, 0, 5);
		gbc_label_44.gridx = 16;
		gbc_label_44.gridy = 2;
		panel.add(label_44, gbc_label_44);
		
		JLabel label_45 = new JLabel("");
		GridBagConstraints gbc_label_45 = new GridBagConstraints();
		gbc_label_45.fill = GridBagConstraints.BOTH;
		gbc_label_45.insets = new Insets(0, 0, 0, 5);
		gbc_label_45.gridx = 17;
		gbc_label_45.gridy = 2;
		panel.add(label_45, gbc_label_45);
		
		JLabel label_46 = new JLabel("");
		GridBagConstraints gbc_label_46 = new GridBagConstraints();
		gbc_label_46.fill = GridBagConstraints.BOTH;
		gbc_label_46.insets = new Insets(0, 0, 0, 5);
		gbc_label_46.gridx = 18;
		gbc_label_46.gridy = 2;
		panel.add(label_46, gbc_label_46);
		
		JLabel label_47 = new JLabel("");
		GridBagConstraints gbc_label_47 = new GridBagConstraints();
		gbc_label_47.fill = GridBagConstraints.BOTH;
		gbc_label_47.insets = new Insets(0, 0, 0, 5);
		gbc_label_47.gridx = 19;
		gbc_label_47.gridy = 2;
		panel.add(label_47, gbc_label_47);
		
		JLabel label_48 = new JLabel("");
		GridBagConstraints gbc_label_48 = new GridBagConstraints();
		gbc_label_48.fill = GridBagConstraints.BOTH;
		gbc_label_48.insets = new Insets(0, 0, 0, 5);
		gbc_label_48.gridx = 20;
		gbc_label_48.gridy = 2;
		panel.add(label_48, gbc_label_48);
		
		JLabel label_49 = new JLabel("");
		GridBagConstraints gbc_label_49 = new GridBagConstraints();
		gbc_label_49.fill = GridBagConstraints.BOTH;
		gbc_label_49.insets = new Insets(0, 0, 0, 5);
		gbc_label_49.gridx = 21;
		gbc_label_49.gridy = 2;
		panel.add(label_49, gbc_label_49);
		
		JLabel label_50 = new JLabel("");
		GridBagConstraints gbc_label_50 = new GridBagConstraints();
		gbc_label_50.fill = GridBagConstraints.BOTH;
		gbc_label_50.insets = new Insets(0, 0, 0, 5);
		gbc_label_50.gridx = 22;
		gbc_label_50.gridy = 2;
		panel.add(label_50, gbc_label_50);
		
		JLabel label_51 = new JLabel("");
		GridBagConstraints gbc_label_51 = new GridBagConstraints();
		gbc_label_51.fill = GridBagConstraints.BOTH;
		gbc_label_51.insets = new Insets(0, 0, 0, 5);
		gbc_label_51.gridx = 23;
		gbc_label_51.gridy = 2;
		panel.add(label_51, gbc_label_51);
		
		JLabel label_52 = new JLabel("");
		GridBagConstraints gbc_label_52 = new GridBagConstraints();
		gbc_label_52.fill = GridBagConstraints.BOTH;
		gbc_label_52.insets = new Insets(0, 0, 0, 5);
		gbc_label_52.gridx = 24;
		gbc_label_52.gridy = 2;
		panel.add(label_52, gbc_label_52);
		
		JLabel label_53 = new JLabel("");
		GridBagConstraints gbc_label_53 = new GridBagConstraints();
		gbc_label_53.fill = GridBagConstraints.BOTH;
		gbc_label_53.insets = new Insets(0, 0, 0, 5);
		gbc_label_53.gridx = 25;
		gbc_label_53.gridy = 2;
		panel.add(label_53, gbc_label_53);
		
		JLabel label_54 = new JLabel("");
		GridBagConstraints gbc_label_54 = new GridBagConstraints();
		gbc_label_54.fill = GridBagConstraints.BOTH;
		gbc_label_54.insets = new Insets(0, 0, 0, 5);
		gbc_label_54.gridx = 26;
		gbc_label_54.gridy = 2;
		panel.add(label_54, gbc_label_54);
		
		JLabel label_55 = new JLabel("");
		GridBagConstraints gbc_label_55 = new GridBagConstraints();
		gbc_label_55.fill = GridBagConstraints.BOTH;
		gbc_label_55.insets = new Insets(0, 0, 0, 5);
		gbc_label_55.gridx = 27;
		gbc_label_55.gridy = 2;
		panel.add(label_55, gbc_label_55);
		
		JLabel label_56 = new JLabel("");
		GridBagConstraints gbc_label_56 = new GridBagConstraints();
		gbc_label_56.fill = GridBagConstraints.BOTH;
		gbc_label_56.gridx = 28;
		gbc_label_56.gridy = 2;
		panel.add(label_56, gbc_label_56);
	}
}
