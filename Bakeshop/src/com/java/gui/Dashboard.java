package com.java.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import java.awt.Insets;

import com.java.model.*;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import javax.swing.JTable;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.awt.event.ActionEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.java.config.*;
import javax.swing.JTextField;

public class Dashboard extends JFrame {

	private static final long serialVersionUID = 1L;

	private JFrame frame;

	private User user;
	JPanel mainPanel = new JPanel();
	private ArrayList<Inventory> inventoryList = new ArrayList<Inventory>();
	private JComboBox<String> storeDropdown;
	private JTextField customerName;
	private JTextField phoneNumberField;

	private int LOW_INVENTORY = 30;
	private String COFFEE_INV = "Coffee";
	private String COFFEE_BEAN_INV = "coffee bean";
	

	/**
	 * Create the application.
	 */
	public Dashboard(User user) {
		this.user = user;
		initialize();
		clearDashboardScreen();
	}

	private void clearDashboardScreen() {
		mainPanel.removeAll();
		mainPanel.revalidate();
		mainPanel.repaint();

		try {
			DefaultTableModel model = new DefaultTableModel(new String[] { "Order Id", "Item Name", "Quantity",
					"Item No", "Price", "Total Amount", "Customer Name", "Phone No", "Status" }, 0);

			Connection connection = DatabaseConnection.getDbConnection();

			PreparedStatement st = (PreparedStatement) connection.prepareStatement(
					"select orderId,itemNames,quantity,itemNo,price,totalAmount,customerName,customerPhone,orderStatus from Orders");

			ResultSet rs = st.executeQuery();

			inventoryList.clear();
			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9) });
			}

			JPanel orderPanel = new JPanel(new BorderLayout());
			orderPanel.setBounds(94, 70, 830, 327);

			JTable table = new JTable();
			table.setModel(model);

			table.setFillsViewportHeight(true);
			JScrollPane scrollTable = new JScrollPane(table);
			orderPanel.add(scrollTable, BorderLayout.CENTER);

			mainPanel.add(orderPanel);
			mainPanel.revalidate();
			mainPanel.repaint();

		} catch (Exception e) {

		}

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(119, 136, 153));
		frame.setBounds(10, 10, 1450, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1432, 100);
		panel.setBackground(Color.ORANGE);
		frame.getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 1043, 112, 0 };
		gbl_panel.rowHeights = new int[] { 30, 29, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblNewLabel = new JLabel("Bake Shop");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setForeground(Color.WHITE);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		panel.add(lblNewLabel, gbc_lblNewLabel);

		JLabel employeeName = new JLabel("New label");
		employeeName.setForeground(new Color(255, 255, 255));
		employeeName.setFont(new Font("Tahoma", Font.PLAIN, 20));
		employeeName.setBounds(657, 131, 315, 16);
		frame.getContentPane().add(employeeName);
		employeeName.setText("Welcome " + this.user.getEmpName());

		JButton dashboardBtn = new JButton("Dashboard");
		dashboardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				initialize();
				clearDashboardScreen();
			}
		});
		dashboardBtn.setBounds(25, 225, 113, 45);
		frame.getContentPane().add(dashboardBtn);

		mainPanel.setBounds(173, 191, 1225, 435);
		frame.getContentPane().add(mainPanel);
		mainPanel.setLayout(null);

		JButton createOrderBtn = new JButton("Create Order");
		JButton setStore = new JButton("Select");

		createOrderBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainPanel.removeAll();
				storeDropdown = new JComboBox<String>();
				storeDropdown.setForeground(new Color(100, 149, 237));
				storeDropdown.setBackground(new Color(30, 144, 255));
				storeDropdown.setBounds(52, 47, 236, 36);
				storeDropdown.setVisible(true);
				try {

					Connection connection = DatabaseConnection.getDbConnection();

					PreparedStatement st = (PreparedStatement) connection
							.prepareStatement("select storeId from user_store where employeId = ?");

					st.setInt(1, user.getEmpId());

					ResultSet rs = st.executeQuery();

					while (rs.next()) {

						int tempStoreId = rs.getInt(1);
						storeDropdown.addItem(Integer.toString(tempStoreId));
					}
					storeDropdown.setSelectedIndex(0);
					mainPanel.add(storeDropdown);

					setStore.setBounds(316, 47, 128, 36);
					mainPanel.add(setStore);
					mainPanel.revalidate();
					mainPanel.repaint();
				} catch (SQLException sqlException) {
					System.out.println(sqlException.toString());
					sqlException.printStackTrace();
				}
			}

		});

		createOrderBtn.setBounds(1236, 141, 133, 37);
		frame.getContentPane().add(createOrderBtn);

		JButton logoutBtn = new JButton("Logout");
		logoutBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				frame.dispose();
				user = null;
				Login login = new Login();
			}
		});
		
		frame.getContentPane().add(logoutBtn);
		


		
		
		if( user.getEmpRole().equalsIgnoreCase("owner") ) {
			
			JButton reportButton = new JButton("Reports");
			reportButton.setBounds(25, 318, 113, 45);
			frame.getContentPane().add(reportButton);
			logoutBtn.setBounds(25, 518, 113, 45);
			
			reportButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					mainPanel.removeAll();
					mainPanel.revalidate();
					mainPanel.repaint();
					showReportPanel();
					
				}

				private void showReportPanel() {
					
					JButton btnNewButton = new JButton("Low Inventory");
					btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {

							Connection connection = DatabaseConnection.getDbConnection();

							try {
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(
										"select inv.itemID,itemName,quantity,st.storeid, concat(StreetAddress,' ', city,' ',suburb,' ',postal) as address, ContactNumber from "
												+ "	store_inventory s join inventory inv on s.itemId = inv.itemId join store st on s.storeId = st.storeid "
												+ "where s.quantity < ? ");

								st.setInt(1, LOW_INVENTORY);

								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "raport.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(6);
									tbl.addCell("Item Id");
									tbl.addCell("Item Name");
									tbl.addCell("Quantity");
									tbl.addCell("Store Id");
									tbl.addCell("Address");
									tbl.addCell("Store Contact Number");
									
									while (rs.next()) {
										tbl.addCell( rs.getString(1) );
										tbl.addCell( rs.getString(2) );
										tbl.addCell( Integer.toString(rs.getInt(3) ) );
										tbl.addCell( Integer.toString(rs.getInt(4) ) );
										tbl.addCell( rs.getString(5) );
										tbl.addCell( rs.getString(6) );
									}

									doc.add(tbl);
									doc.close();
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});
					
					
					JButton coffeeBtn = new JButton("Coffee Order");
					coffeeBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "select orderId,storeId,quantity,price,itemNames from orders where itemNames like \"%" + COFFEE_INV +"%\" ";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "Coffee.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(5);
									tbl.addCell("Order Id");
									tbl.addCell("Item Name");
									tbl.addCell("Store Id");
									tbl.addCell("Quantity");
									tbl.addCell("Price");
									
									while (rs.next()) {
										tbl.addCell(Integer.toString( rs.getInt(1) ));
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(COFFEE_INV);
										String[] quantityString = rs.getString(3).split(",");
										String[] itemNames = rs.getString(5).split(",");
										String[] priceList = rs.getString(4).split(",");
										int itemIndex = -1;
										for(int i=0; i < itemNames.length;i++) {
											String item = itemNames[i].trim();
											if( item.equalsIgnoreCase(COFFEE_INV) ) {
												itemIndex = i;
												break;
											}
											
										}
										
										String Qunatity = quantityString[itemIndex];
										String Price = priceList[itemIndex];
										
										tbl.addCell( Qunatity);
										tbl.addCell( Price );

									}

									doc.add(tbl);
									doc.close();
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					
					JButton coffeeBeanBtn = new JButton("Coffee Beans");
					coffeeBeanBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "SELECT sum(CAST(quantity AS signed))as quantity,storeId  from orders group by storeId";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "CoffeeBeans.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(3);

									tbl.addCell("Item Name");
									tbl.addCell("Store Id");
									tbl.addCell("Quantity");
									
									while (rs.next()) {
										tbl.addCell("Coffe Beans");
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(Integer.toString( rs.getInt(1) ));


									}

									doc.add(tbl);
									doc.close();
									JOptionPane.showMessageDialog(frame, "Downloaded");
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					JButton coffeeTypeBtn = new JButton("Coffee Type");
					coffeeBeanBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "select orderId,storeId,quantity,price,itemNames from orders where itemNames like \"%" + COFFEE_BEAN_INV +"%\" ";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "CoffeeBeans.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(5);
									tbl.addCell("Order Id");
									tbl.addCell("Item Name");
									tbl.addCell("Store Id");
									tbl.addCell("Quantity");
									tbl.addCell("Price");
									
									while (rs.next()) {
										tbl.addCell(Integer.toString( rs.getInt(1) ));
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(COFFEE_BEAN_INV);
										String[] quantityString = rs.getString(3).split(",");
										String[] itemNames = rs.getString(5).split(",");
										String[] priceList = rs.getString(4).split(",");
										int itemIndex = -1;
										for(int i=0; i < itemNames.length;i++) {
											String item = itemNames[i].trim();
											if( item.equalsIgnoreCase(COFFEE_BEAN_INV) ) {
												itemIndex = i;
												break;
											}
											
										}
										
										String Qunatity = quantityString[itemIndex];
										String Price = priceList[itemIndex];
										
										tbl.addCell( Qunatity);
										tbl.addCell( Price );

									}

									doc.add(tbl);
									doc.close();
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					JButton mostSaleBtn = new JButton("Most Sale");
					coffeeBeanBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "select orderId,storeId,quantity,price,itemNames from orders where itemNames like \"%" + COFFEE_BEAN_INV +"%\" ";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "CoffeeBeans.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(5);
									tbl.addCell("Order Id");
									tbl.addCell("Item Name");
									tbl.addCell("Store Id");
									tbl.addCell("Quantity");
									tbl.addCell("Price");
									
									while (rs.next()) {
										tbl.addCell(Integer.toString( rs.getInt(1) ));
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(COFFEE_BEAN_INV);
										String[] quantityString = rs.getString(3).split(",");
										String[] itemNames = rs.getString(5).split(",");
										String[] priceList = rs.getString(4).split(",");
										int itemIndex = -1;
										for(int i=0; i < itemNames.length;i++) {
											String item = itemNames[i].trim();
											if( item.equalsIgnoreCase(COFFEE_BEAN_INV) ) {
												itemIndex = i;
												break;
											}
											
										}
										
										String Qunatity = quantityString[itemIndex];
										String Price = priceList[itemIndex];
										
										tbl.addCell( Qunatity);
										tbl.addCell( Price );

									}

									doc.add(tbl);
									doc.close();
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					JButton storeSaleBtn = new JButton("Store Sale");
					storeSaleBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "select sum(totalAmount),storeId as amount from orders where createdOn BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() group by storeId";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "storesales.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(2);
									tbl.addCell("Store Id");
									tbl.addCell("Total Amount");

									
									while (rs.next()) {
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(Integer.toString( rs.getInt(1) ));

									}

									doc.add(tbl);
									doc.close();
									JOptionPane.showMessageDialog(frame, "Downloaded");
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					
					JButton foodItemBtn = new JButton("Food Item");
					coffeeBeanBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Connection connection = DatabaseConnection.getDbConnection();

							try {
								String queryStatement = "select orderId,storeId,quantity,price,itemNames from orders where itemNames like \"%" + COFFEE_BEAN_INV +"%\" ";
								PreparedStatement st = (PreparedStatement) connection.prepareStatement(queryStatement);


								ResultSet rs = st.executeQuery();

								Document doc = new Document();
								try {
									String path = "C:\\PDF\\";
									
									PdfWriter.getInstance(doc, new FileOutputStream(path + "CoffeeBeans.pdf"));
									doc.open();
									PdfPTable tbl = new PdfPTable(5);
									tbl.addCell("Order Id");
									tbl.addCell("Item Name");
									tbl.addCell("Store Id");
									tbl.addCell("Quantity");
									tbl.addCell("Price");
									
									while (rs.next()) {
										tbl.addCell(Integer.toString( rs.getInt(1) ));
										tbl.addCell(Integer.toString( rs.getInt(2) ));
										tbl.addCell(COFFEE_BEAN_INV);
										String[] quantityString = rs.getString(3).split(",");
										String[] itemNames = rs.getString(5).split(",");
										String[] priceList = rs.getString(4).split(",");
										int itemIndex = -1;
										for(int i=0; i < itemNames.length;i++) {
											String item = itemNames[i].trim();
											if( item.equalsIgnoreCase(COFFEE_BEAN_INV) ) {
												itemIndex = i;
												break;
											}
											
										}
										
										String Qunatity = quantityString[itemIndex];
										String Price = priceList[itemIndex];
										
										tbl.addCell( Qunatity);
										tbl.addCell( Price );

									}

									doc.add(tbl);
									doc.close();
								} catch (Exception ex) {
									ex.printStackTrace();
								} 

								

							} catch (Exception exp) {
								// TODO Auto-generated catch block
								exp.printStackTrace();
							}
						}
					});
					
					foodItemBtn.setBounds(790, 20, 125, 45);
					mainPanel.add(foodItemBtn);
					
					
					storeSaleBtn.setBounds(660, 20, 125, 45);
					mainPanel.add(storeSaleBtn);
					
					mostSaleBtn.setBounds(530, 20, 125, 45);
					mainPanel.add(mostSaleBtn);
					
					coffeeTypeBtn.setBounds(400, 20, 125, 45);
					mainPanel.add(coffeeTypeBtn);
					
					coffeeBeanBtn.setBounds(270, 20, 125, 45);
					mainPanel.add(coffeeBeanBtn);
					
					btnNewButton.setBounds(10, 20, 125, 45);
					mainPanel.add(btnNewButton);
					
					
					coffeeBtn.setBounds(140, 20, 125, 45);
					mainPanel.add(coffeeBtn);
					
					
				}
			});

			
		}
		else {
			
			logoutBtn.setBounds(25, 318, 113, 45);
		}

		
		
		frame.setVisible(true);
		
		setStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int setStore = Integer.parseInt(storeDropdown.getSelectedItem().toString());
				user.setStoreId(setStore);
				mainPanel.removeAll();
				mainPanel.revalidate();
				mainPanel.repaint();

				customerName = new JTextField();
				customerName.setBounds(1008, 100, 195, 36);
				mainPanel.add(customerName);
				customerName.setColumns(10);

				JLabel customerNameLabel = new JLabel("New label");
				customerNameLabel.setForeground(new Color(0, 0, 0));
				customerNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
				customerNameLabel.setBounds(1008, 70, 195, 36);
				mainPanel.add(customerNameLabel);
				customerNameLabel.setText("Enter Customer Name");

				JLabel phoneNumberLabel = new JLabel("New label");
				phoneNumberLabel.setForeground(new Color(0, 0, 0));
				phoneNumberLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
				phoneNumberLabel.setBounds(1008, 140, 195, 36);
				mainPanel.add(phoneNumberLabel);
				phoneNumberLabel.setText("Enter Customer Phone");

				phoneNumberField = new JTextField();
				phoneNumberField.setBounds(1008, 180, 195, 36);
				mainPanel.add(phoneNumberField);
				phoneNumberField.setColumns(10);

				mainPanel.revalidate();
				mainPanel.repaint();
				generateMenuItems();
			}

			private void generateMenuItems() {

				try {
					DefaultTableModel model = new DefaultTableModel(
							new String[] { "Item No", "Item Name", "Price", "Available", "Ordered" }, 0) {

						private static final long serialVersionUID = 1L;

						@Override
						public boolean isCellEditable(int row, int column) {
							// all cells false
							return column == 4;
						}
					};

					Connection connection = DatabaseConnection.getDbConnection();

					PreparedStatement st = (PreparedStatement) connection.prepareStatement(
							"select st.itemId,itemName,price,quantity from inventory inv join store_inventory st on inv.itemId = st.itemId and st.storeId = ?");

					st.setInt(1, user.getStoreId());

					ResultSet rs = st.executeQuery();

					inventoryList.clear();
					while (rs.next()) {
						model.addRow(new Object[] { rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), 0 });
					}

					JPanel orderPanel = new JPanel(new BorderLayout());
					orderPanel.setBounds(94, 70, 830, 327);

					JTable table = new JTable();
					table.setModel(model);

					table.setFillsViewportHeight(true);
					JScrollPane scrollTable = new JScrollPane(table);
					orderPanel.add(scrollTable, BorderLayout.CENTER);

					mainPanel.add(orderPanel);
					JButton confirmBtn = new JButton("Confirm");
					confirmBtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							int inventoryRowCount = table.getModel().getRowCount();
							String tempItemNames = "";
							String tempItemNo = "";
							String tempItemQuantity = "";
							String tempItemPrice = "";
							int totalAmount = 0;
							for (int i = 0; i < inventoryRowCount; i++) {

								int tempAvailable = (int) table.getModel().getValueAt(i, 3);
								int tempOrder = Integer.parseInt(table.getModel().getValueAt(i, 4).toString());

								if (tempAvailable < tempOrder) {
									String errorMessage = table.getModel().getValueAt(i, 1) + "is Out of Stock";
									JOptionPane.showMessageDialog(frame, errorMessage);
									break;
								}

								if (tempOrder > 0) {
									tempItemNo += table.getModel().getValueAt(i, 0) + ",";
									tempItemNames += table.getModel().getValueAt(i, 1) + ",";
									tempItemQuantity += table.getModel().getValueAt(i, 4) + ",";
									tempItemPrice += (Integer.parseInt(table.getModel().getValueAt(i, 4).toString())
											* (int) table.getModel().getValueAt(i, 2)) + ",";
									totalAmount += Integer.parseInt(table.getModel().getValueAt(i, 4).toString())
											* (int) table.getModel().getValueAt(i, 2);
								}
								// updateOrderDatabase(user.getStoreId(),user.getEmpId());
							}

							DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
							LocalDateTime now = LocalDateTime.now();
							String createOn = dtf.format(now);
							String orderStatus = "Preparing";
							String customerTempName = customerName.getText();
							String customerPhoneNumber = phoneNumberField.getText();
							System.out.println(user.getStoreId());
							System.out.println(user.getEmpId());
							System.out.println(tempItemNames);
							System.out.println(tempItemNo);
							System.out.println(tempItemQuantity);
							System.out.println(tempItemPrice);
							System.out.println(totalAmount);
							System.out.println(createOn);
							System.out.println(orderStatus);
							System.out.println(customerTempName);
							System.out.println(customerPhoneNumber);

							updateOrderDatabase(user.getStoreId(), user.getEmpId(), tempItemNames, tempItemNo,
									tempItemQuantity, tempItemPrice, totalAmount, createOn, customerTempName,
									orderStatus, customerPhoneNumber);

						}

						private void updateOrderDatabase(int storeId, int empId, String tempItemNames,
								String tempItemNo, String tempItemQuantity, String tempItemPrice, int totalAmount,
								String createOn, String customerTempName, String orderStatus,
								String customerPhoneNumber) {
							try {

								Connection connection = DatabaseConnection.getDbConnection();
								PreparedStatement stmt = connection.prepareStatement(
										"insert into Orders(storeId,employeId,itemNames,itemNo,quantity,price,totalAmount,createdOn,customerName,orderStatus,customerPhone) values(?,?,?,?,?,?,?,?,?,?,?);");

								stmt.setInt(1, storeId);
								stmt.setInt(2, empId);
								stmt.setString(3, tempItemNames);
								stmt.setString(4, tempItemNo);
								stmt.setString(5, tempItemQuantity);
								stmt.setString(6, tempItemPrice);
								stmt.setInt(7, totalAmount);

								stmt.setTimestamp(8, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
								stmt.setString(9, customerTempName);
								stmt.setString(10, orderStatus);
								stmt.setString(11, customerPhoneNumber);
								int i = stmt.executeUpdate();
								System.out.println(i + " records inserted");

								// PreparedStatement updateQuantity = connection.prepareStatement("update
								// store_inventory set quantity = ? where storeId = ? and itemId = ?");

								clearDashboardScreen();
							} catch (Exception e) {

							}

						}

					});
					confirmBtn.setBounds(1081, 336, 119, 38);
					mainPanel.add(confirmBtn);
					mainPanel.revalidate();
					mainPanel.repaint();

					for (Inventory inv : inventoryList) {
						System.out.println(inv);
					}

				} catch (SQLException sqlException) {
					sqlException.printStackTrace();
				}

			}
		});

	}
}
